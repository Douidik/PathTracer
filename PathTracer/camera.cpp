#include "camera.h"

using std::vector;

namespace raycasting
{
	raycasting::camera::camera(const vector3 &origin, const vector3 &direction, double angle, int width, int height, double dof, int ssaa = 2) :
		origin(origin), direction(direction), angle(angle), width(width), height(height), cam_width(ssaa * width), cam_height(ssaa * height), img(width, height), dof(dof), ssaa_coeff(ssaa)
	{
		double focal_length = (direction - origin).magnitude();
		double base_width = 2 * focal_length*tan(angle / 2);
		double base_height = base_width*((double)height / width);

		//position the camera origin at (0,0,0), normal aligned with z-axis
		vector3 temp_origin = vector3(0, 0, 0);
		vector3 temp_direction = vector3(0, 0, focal_length);
		vector3 temp_up = vector3::Y_AXIS;
		screen = vector<vector<vector3>>(cam_width, vector<vector3>(cam_height));
		vector3 top_left = temp_direction - vector3(base_width / 2, -base_height / 2, 0);
		double step_w = base_width / (cam_width - 1);
		double step_h = base_height / (cam_height - 1);
		for (int i = 0; i < cam_width; ++i)
			for (int j = 0; j < cam_height; ++j)
				screen[i][j] = top_left + (step_w * i)*vector3::X_AXIS - (step_h*j)*vector3::Y_AXIS;

		//rotate the camera to the correct position
		vector3 dir = (direction - origin).normalize();
		vector3 rotation_axis = (vector3::Z_AXIS % dir).normalize();
		double rotation_angle = temp_direction.angle(dir);
		for (int i = 0; i < cam_width; ++i)
			for (int j = 0; j < cam_height; ++j)
				screen[i][j] = screen[i][j].rotate(rotation_axis, rotation_angle);
		up = temp_up.rotate(rotation_axis, rotation_angle);
	}
}
