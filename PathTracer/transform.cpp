#include "transform.h"

using std::initializer_list;

namespace raycasting
{
	triangle transform::applyTransforms(const triangle &tr, const initializer_list<const transform*> &transforms)
	{
		triangle result = tr;
		for (auto &tf : transforms)
			result = tf->applyTransform(result);
		return result;
	}

	scaling::scaling(double sf) : scale_factor(sf) { }

	triangle scaling::applyTransform(const triangle &tr) const
	{
		vertex a = tr.getA(), b = tr.getB(), c = tr.getC();

		vector3 a_new_coords = a.coords*scale_factor;
		vector3 b_new_coords = b.coords*scale_factor;
		vector3 c_new_coords = c.coords*scale_factor;

		return triangle(vertex(a_new_coords, vertex_data(a.data.normal, a.data.uv)), vertex(b_new_coords, vertex_data(b.data.normal, b.data.uv)), vertex(c_new_coords, vertex_data(c.data.normal, c.data.uv)), tr.mat);
	}

	rotation::rotation(const vector3 &axis, double angle) : axis(axis), angle(angle) { }

	triangle rotation::applyTransform(const triangle &tr) const
	{
		vertex a = tr.getA(), b = tr.getB(), c = tr.getC();

		vector3 a_new_coords = a.coords.rotate(axis, angle);
		vector3 b_new_coords = b.coords.rotate(axis, angle);
		vector3 c_new_coords = c.coords.rotate(axis, angle);

		//also rotate the normals
		vector3 a_new_normal = a.data.normal.rotate(axis, angle);
		vector3 b_new_normal = b.data.normal.rotate(axis, angle);
		vector3 c_new_normal = c.data.normal.rotate(axis, angle);

		return triangle(vertex(a_new_coords, vertex_data(a_new_normal, a.data.uv)), vertex(b_new_coords, vertex_data(b_new_normal, b.data.uv)), vertex(c_new_coords, vertex_data(c_new_normal, c.data.uv)), tr.mat);
	}

	translation::translation(const vector3 &lc) : local_center(lc) { }

	triangle translation::applyTransform(const triangle &tr) const
	{
		vertex a = tr.getA(), b = tr.getB(), c = tr.getC();

		vector3 a_new_coords = a.coords + local_center;
		vector3 b_new_coords = b.coords + local_center;
		vector3 c_new_coords = c.coords + local_center;

		return triangle(vertex(a_new_coords, vertex_data(a.data.normal, a.data.uv)), vertex(b_new_coords, vertex_data(b.data.normal, b.data.uv)), vertex(c_new_coords, vertex_data(c.data.normal, c.data.uv)), tr.mat);
	}
}
