#pragma once
#include <tuple>
#include "shape.h"

using namespace raycasting;

namespace util
{
	std::tuple<shape::intersection, double> intersectObjects(const std::vector<const shape*>&, const ray&, bool, bool);
	const wchar_t* toWcharPtr(const char*);
}
