#pragma once
#include "vector3.h"
#include "triangle.h"

namespace raycasting
{
	class transform
	{
	protected:
		virtual triangle applyTransform(const triangle&) const = 0;
	public:
		static triangle applyTransforms(const triangle&, const std::initializer_list<const transform*>&);
	};

	class scaling : public transform
	{
	private:
		double scale_factor;
	public:
		scaling(double);
		triangle applyTransform(const triangle&) const override;
	};

	class rotation : public transform
	{
	private:
		vector3 axis;
		double angle;
	public:
		rotation(const vector3&, double);
		triangle applyTransform(const triangle&) const override;
	};

	class translation : public transform
	{
	private:
		vector3 local_center;
	public:
		translation(const vector3&);
		triangle applyTransform(const triangle&) const override;
	};
}
