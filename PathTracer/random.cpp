#include <algorithm>
#include "random.h"

using std::max;
using raycasting::vector3;
using namespace util;

namespace util
{
	namespace random
	{
		std::random_device rd;
		thread_local std::default_random_engine rng(rd());

		vector3 randCone(double theta, const vector3 &vec)
		{
			std::uniform_real_distribution<double> phi_distribution(0, 2 * M_PI);
			std::uniform_real_distribution<double> z_distribution(cos(theta), 1);
			double phi = phi_distribution(rng);
			double z = z_distribution(rng);
			vector3 rand_vec = vector3(sqrt(1 - (z*z))*cos(phi), sqrt(1 - (z*z))*sin(phi), z);
			//find rotation axis and angle
			vector3 rotation_axis = (vector3::Z_AXIS % vec.normalize()).normalize();
			double rotation_angle = vector3::Z_AXIS.angle(vec);
			//rotate
			return rand_vec.rotate(rotation_axis, rotation_angle);
		}

		vector3 randHemisphere(double u1, double u2, const vector3 &vec)
		{
			const double r = sqrt(u1);
			const double theta = 2 * M_PI * u2;
			const double x = r * cos(theta);
			const double y = r * sin(theta);
			vector3 rand_vec = vector3(x, y, sqrt(max(0.00, 1 - u1)));
			//find rotation axis and angle
			vector3 rotation_axis = (vector3::Z_AXIS % vec.normalize()).normalize();
			double rotation_angle = vector3::Z_AXIS.angle(vec);
			//rotate
			return rand_vec.rotate(rotation_axis, rotation_angle);
		}

		double randomNumUniform(double a, double b)
		{
			std::uniform_real_distribution<double> distribution(a, b);
			return distribution(rng);
		}

		double randNumExp(double exp)
		{
			std::exponential_distribution<double> distribution(exp);
			return distribution(rng);
		}

		vector3 randSphere()
		{
			double theta = 2 * M_PI * randomNumUniform(0, 1);
			double phi = acos(1 - 2 * randomNumUniform(0, 1));
			return vector3(sin(phi) * cos(theta), sin(phi) * sin(theta), cos(phi));
		}
	}
}