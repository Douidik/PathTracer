#include "uv_coords.h"

namespace raycasting
{
	raycasting::uv_coords::uv_coords(double u, double v) : u(u), v(v) { }

	uv_coords raycasting::uv_coords::operator*(double k) const
	{
		return uv_coords(u*k, v*k);
	}

	uv_coords uv_coords::operator+(const uv_coords &uv) const
	{
		return uv_coords(u + uv.u, v + uv.v);
	}

	uv_coords raycasting::operator*(double k, const uv_coords &uv)
	{
		return uv*k;
	}
}
