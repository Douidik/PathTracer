#pragma once
#include "FBX\fbxsdk.h"
#include "triangle.h"
#include "sphere.h"
#include "transform.h"

namespace raycasting
{
	class mesh : public shape
	{
	private:
		std::vector<triangle> triangles;
		sphere bounding_sphere;
	public:
		mesh() = default;
		mesh(const std::initializer_list<triangle>&, const std::initializer_list<const transform*>&);
		mesh(const char*, const std::initializer_list<const transform*>&, const material&, bool, bool = false);
		std::vector<triangle> transformTriangles(const std::vector<triangle>&, const std::initializer_list<const transform*>&) const;
		sphere calculateBoundingSphere() const;
		intersection intersect(const ray&, bool) const override;
		std::vector<vector3> getAllPoints() const;
	};
}
