#pragma once
#include <memory>
#include "color.h"
#include "texture.h"

namespace raycasting
{
	struct material
	{
		color col;
		double transparency, reflectivity, refraction_index, emissivity, diffuseness, scattering, smoothness;
		std::shared_ptr<texture> tex;
		material() = default;
		material(const color&, double, double, double, double, double, double, double, std::shared_ptr<texture>);
	};
}
