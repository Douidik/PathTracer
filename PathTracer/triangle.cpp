#include "triangle.h"

const double DELTA = 0.0000001;

namespace raycasting
{
	triangle::triangle(const vertex &a, const vertex &b, const vertex &c, const material &mat) : a(a), b(b), c(c)
	{
		interpolate_normals = false;
		this->mat = mat;
	}

	vertex triangle::getA() const
	{
		return a;
	}

	vertex triangle::getB() const
	{
		return b;
	}

	vertex triangle::getC() const
	{
		return c;
	}

	shape::intersection triangle::intersect(const ray &ray, bool interpolate_normals = false) const
	{
		vector3 r_d = ray.direction, r_o = ray.origin;
		vector3 k = r_o - a.coords, e1 = b.coords - a.coords, e2 = c.coords - a.coords;
		vector3 d_e2 = r_d % e2, k_e1 = k % e1;
		double det = d_e2 * e1;
		if (det >= -DELTA && det <= DELTA)
			return intersection(); //no intersection 
		else
		{
			bool inside = det < 0;
			double inv_det = 1 / det, u = inv_det * (d_e2*k);
			if (u < 0 || u > 1)
				return intersection();
			else
			{
				double v = inv_det * (k_e1*r_d);
				if (v < 0 || u + v > 1)
					return intersection();
				else
				{
					//there is an intersection, compute t
					double t = inv_det * (k_e1*e2);
					vector3 point = r_o + t * r_d;
					vertex_data intrs_data = interpolateData(a, b, c, u, v, 1 - u - v);
					vector3 temp_normal = interpolate_normals ? intrs_data.normal.normalize() : e1 % e2.normalize();
					vector3 normal = inside ? -temp_normal : temp_normal;
					return intersection(point, normal, mat, t > DELTA, inside, intrs_data.uv);
				}
			}
		}
		return intersection();
	}

	vertex_data triangle::interpolateData(const vertex &a, const vertex &b, const vertex &c, double u, double v, double w) const
	{
		vector3 normal = w * a.data.normal + u * b.data.normal + v * c.data.normal;
		uv_coords uv = w * a.data.uv + u * b.data.uv + v * c.data.uv;
		return vertex_data(normal, uv);
	}
}
