#pragma once
#include <vector>
#include <memory>
#include <thread>
#include "vector3.h"
#include "camera.h"
#include "shape.h"
#include "random.h"
#include "realtime_drawing_manager.h"
#include "directx_manager.h"
#include "pathtracer.h"
#include "raytracer.h"

namespace raycasting
{
	class pathtracing_raytracer : public pathtracer, raytracer
	{
	public:
		pathtracing_raytracer() = default;
		pathtracing_raytracer(const camera&, const std::initializer_list<const shape*>&, const material&, std::shared_ptr<realtime_drawing_manager>, const std::vector<point_light*>&, double);
		virtual color radiance(int, int, const ray&, const material&, int) const override;
	};
}
