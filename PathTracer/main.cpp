#pragma once
#include <iostream>
#include <cstdio>
#include <memory>
#include <Windows.h>
#include "ray.h"
#include "sphere.h"
#include "triangle.h"
#include "mesh.h"
#include "camera.h"
#include "pathtracer.h"
#include "raytracer.h"
#include "transform.h"
#include "directx_manager.h"
#include "pathtracing_raytracer.h"
#include "point_light.h"

using namespace raycasting;
using std::vector;

//material: transparency, reflectivity, refraction_index, emissivity, diffuseness, scattering, smoothness
const material air(color(1, 1, 1), 1, 0, 1, 0, 0, 0, 0, nullptr);
const material fog(color(0.5, 0.5, 0.5), 1, 0, 1, 0, 0, 0.005, 0, nullptr);
const material red_diffuse(color(1, 0.5, 0.5), 0, 0, 0, 0, 1, 0, 0, nullptr);
const material red_smooth(color(1, 0.5, 0.5), 0, 0, 0, 0, 1, 0, 0.8, nullptr);
const material green_diffuse(color(0.5, 1, 0.5), 0, 0, 0, 0, 1, 0, 0, nullptr);
const material blue_diffuse(color(0.6, 0.6, 0.9), 0, 0, 0, 0, 1, 0, 0, nullptr);
const material gray_diffuse(color(0.4, 0.4, 0.4), 0, 0, 0, 0, 1, 0, 0, nullptr);
const material white_diffuse(color(1, 1, 1), 0, 0, 0, 0, 1, 0, 0, nullptr);
const material beige_diffuse(color(0.95, 0.94, 0.86), 0, 0, 0, 0, 1, 0, 0, nullptr);
const material white_metallic(color(0.4, 0.4, 0.4), 0, 0, 0.1, 0, 0.9, 0, 0.82, nullptr);
const material yellow_diffuse(color(1, 1, 0), 0, 0, 0, 0, 1, 0, 0, nullptr);
const material red_ruby(color(1, 0.3, 0.3), 1, 0, 1.76, 0, 0, 0, 0, nullptr);
const material blue_diamond(color(0.7, 0.7, 0.9), 0.9, 0, 2.41, 0, 0, 0, 0, nullptr);
const material mirror(color(0.8, 0.8, 0.8), 0, 1, 0, 0, 0, 0, 0, nullptr);
const material teapot_mat(color(1, 1, 0), 0, 0.5, 1.52, 0, 0.5, 0, 0, nullptr);
const material glass_white(color(0.8, 0.8, 0.8), 0.5, 0.2, 1.7, 0, 0.4, 0, 0.5, nullptr);
const material glass_blue(color(0.8, 0.8, 1.0), 0.5, 0.2, 1.7, 0, 0.4, 0, 0.5, nullptr);
const material glass_green(color(0.8, 1.0, 0.8), 0.5, 0.2, 1.7, 0, 0.4, 0, 0.5, nullptr);

int main()
{
	int width = 320, height = 240;
	int ssaa_coeff = 2;

	auto drm_sptr = std::make_shared<directx_manager>(width, height, GetModuleHandle(NULL));

	camera cam(vector3(0, 0, 0), vector3(1, 0, 0), M_PI / 2.5, width, height, 0, ssaa_coeff);
	sphere sphere1(vector3(150, -30, 20), 18, mirror);
	sphere sphere2(vector3(125, -30, -22), 18, glass_white);
	translation t1(vector3(120, -25, 1));
	scaling s1(1.0);
	rotation r1(vector3(1, 0, 0), -M_PI_2);
	rotation r2(vector3(0, 1, 0), M_PI_2);
	sphere sph1(vector3(10200, 0, 0), 10000, white_diffuse);
	sphere sph2(vector3(100, -10049, 0), 10000, white_diffuse);
	sphere sph3(vector3(100, 10049, 0), 10000, white_diffuse);
	sphere sph4(vector3(100, 0, 10050), 10000, red_diffuse);
	sphere sph5(vector3(100, 0, -10050), 10000, blue_diffuse);
	sphere light1(vector3(150, 60, 0), 20, material(color(1, 1, 1), 0, 0, 0, 5, 0, 0, 0, nullptr));
	translation t2(vector3(140, 49, 0));
	scaling s2(0.6);
	rotation r3(vector3(1, 0, 0), -M_PI_2);
	mesh light("Models/light.fbx", { &s2, &r3, &t2 }, material(color(1, 1, 1), 0, 0, 0, 10, 0, 0, 0, nullptr), false);
	pathtracer pt(cam, { &sph1, &sph2, &sph3, &sph4, &sph5, &light, &sphere1, &sphere2 }, air, drm_sptr);
	pt.render(128, 5, "cornellbox.ppm");
}