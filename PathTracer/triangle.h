#pragma once
#include "shape.h"
#include "vertex.h"

namespace raycasting
{
	class triangle : public shape
	{
	private:
		vertex a, b, c;
	public:
		triangle() = default;
		triangle(const vertex&, const vertex&, const vertex&, const material&);
		vertex getA() const;
		vertex getB() const;
		vertex getC() const;
		intersection intersect(const ray&, bool) const override;
		vertex_data interpolateData(const vertex&, const vertex&, const vertex&, double, double, double) const;
	};
}
