#include <ctime>
#include "vector3.h"

namespace raycasting
{
	const vector3 vector3::X_AXIS = vector3(1, 0, 0);
	const vector3 vector3::Y_AXIS = vector3(0, 1, 0);
	const vector3 vector3::Z_AXIS = vector3(0, 0, 1);

	vector3::vector3(double x, double y, double z) : x(x), y(y), z(z) { }

	double vector3::getX() const
	{
		return x;
	}

	double vector3::getY() const
	{
		return y;
	}

	double vector3::getZ() const
	{
		return z;
	}

	double vector3::magnitude() const
	{
		return sqrt((*this)*(*this));
	}

	vector3 vector3::normalize() const
	{
		double magnitude = this->magnitude();
		if (magnitude != 0)
			return vector3(x / magnitude, y / magnitude, z / magnitude);
		else
			return vector3(0, 0, 0);
	}

	double vector3::distance(const vector3 &vec) const
	{
		return sqrt(sqrDistance(vec));
	}

	double vector3::sqrDistance(const vector3 &vec) const
	{
		double temp_x = vec.x - x;
		double temp_y = vec.y - y;
		double temp_z = vec.z - z;
		return temp_x*temp_x + temp_y*temp_y + temp_z*temp_z;
	}

	double vector3::angle(const vector3 &vec) const
	{
		return acos((*this*vec) / (magnitude()*vec.magnitude()));
	}

	vector3 vector3::operator-() const
	{
		return *this*(-1);
	}

	vector3 vector3::operator+(const vector3 &vec) const
	{
		return vector3(x + vec.x, y + vec.y, z + vec.z);
	}

	vector3 vector3::operator-(const vector3 &vec) const
	{
		return vector3(x - vec.x, y - vec.y, z - vec.z);
	}

	vector3& vector3::operator+=(const vector3 &vec)
	{
		return *this = *this + vec;
	}

	vector3& vector3::operator-=(const vector3 &vec)
	{
		return *this = *this - vec;
	}

	vector3 vector3::operator*(double k) const
	{
		return vector3(x*k, y*k, z*k);
	}

	vector3 & vector3::operator*=(double k)
	{
		return *this = *this*k;
	}

	double vector3::operator*(const vector3 &vec) const
	{
		return x*vec.x + y*vec.y + z*vec.z;
	}

	vector3 vector3::operator%(const vector3 &vec) const
	{
		double xx = y*vec.z - z*vec.y;
		double yy = z*vec.x - x*vec.z;
		double zz = x*vec.y - y*vec.x;
		return vector3(xx, yy, zz);
	}

	vector3 vector3::rotate(const vector3 &axis, double theta) const
	{
		vector3 axis_norm = axis.normalize();
		double cos_theta = cos(theta);
		return *this*cos_theta + (axis_norm%*this)*sin(theta) + axis_norm*(axis_norm*(*this))*(1 - cos_theta);
	}

	vector3 vector3::reflect(const vector3 &vec) const
	{
		return 2 * (*this*vec)*vec - *this;
	}

	vector3 operator*(double k, const vector3 &vec)
	{
		return vec*k;
	}

	std::ostream& operator<<(std::ostream &out, const vector3 &vec)
	{
		out << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")" << std::endl;
		return out;
	}
}