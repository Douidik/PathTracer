#pragma once
#include "image.h"
#include "uv_coords.h"

namespace raycasting
{
	struct texture
	{
		image img;
		texture(const char*);
		color bilinearIntp(double, double);
		color operator[](const uv_coords&);
	};
}
