#pragma once

#include <vector>
#include "color.h"

namespace raycasting
{
	class realtime_drawing_manager
	{
	protected:
		volatile bool running;
	public:
		virtual void start() = 0;
		virtual void draw(const std::vector<std::vector<color>>, int) = 0;
		virtual void stop() = 0;
		virtual void messageloop() = 0;
	};
}
