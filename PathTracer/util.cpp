#include "util.h"

using std::vector;
using std::tuple;
using std::make_tuple;

namespace util
{
	tuple<shape::intersection, double> intersectObjects(const vector<const shape*> &shapes, const ray &curr_ray, bool is_mesh, bool interpolate_normals)
	{
		//find all intersections 
		vector<raycasting::shape::intersection> intersections;
		for (int i = 0; i < (int)shapes.size(); ++i)
		{
			auto intrs = is_mesh ? shapes[i]->intersect(curr_ray, interpolate_normals) : shapes[i]->intersect(curr_ray, shapes[i]->interpolate_normals);
			if (intrs.valid)
				intersections.push_back(intrs);
		}
		size_t intrs_size = intersections.size();
		if (intrs_size == 0) //no intersections
			return make_tuple(shape::intersection(), 0);

		//find nearest intersection
		int index = 0;
		double min_distance = intersections[index].point.sqrDistance(curr_ray.origin);
		for (int i = 1; i < (int)intrs_size; ++i)
		{
			double temp = intersections[i].point.sqrDistance(curr_ray.origin);
			if (temp < min_distance)
			{
				min_distance = temp;
				index = i;
			}
		}
		return make_tuple(intersections[index], min_distance);
	}

	const wchar_t* toWcharPtr(const char *c)
	{
		const size_t cSize = strlen(c) + 1;
		wchar_t* wc = new wchar_t[cSize];
		mbstowcs(wc, c, cSize);
		return wc;
	}
}