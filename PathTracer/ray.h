#pragma once
#include "vector3.h"

namespace raycasting
{
	struct ray
	{
		vector3 origin, direction;
		ray() = default;
		ray(const vector3&, const vector3&);
	};
}