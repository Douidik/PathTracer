#pragma once

//include and link appropriate libraries and headers
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "d3dx10.lib")

#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#include <xnamath.h>
#include <vector>
#include "realtime_drawing_manager.h"
#include "color.h"

namespace raycasting
{
	class directx_manager : public realtime_drawing_manager
	{
	private:
		IDXGISwapChain* swap_chain;
		ID3D11Texture2D* back_buffer;
		ID3D11Device* d3d11_device;
		ID3D11DeviceContext* d3d11_dev_con;
		ID3D11RenderTargetView* render_target_view;
		LPCTSTR wnd_class_name;
		HWND hwnd;
		HINSTANCE h_instance;
		int width, height;
		char* data;
		bool initializeDirect3d11App(HINSTANCE h_instance);
		void releaseObjects();
		bool initializeWindow(HINSTANCE, int, int, int, bool);
		static LRESULT CALLBACK wndProc(HWND, UINT, WPARAM, LPARAM);
	public:
		directx_manager(int, int, HINSTANCE);
		directx_manager(const directx_manager&) = delete;
		directx_manager(directx_manager&&) = default;
		~directx_manager();
		virtual void start() override;
		virtual void draw(const std::vector<std::vector<color>>, int) override;
		virtual void stop() override;
		virtual void messageloop() override;
	};
}
