#include "raytracer.h"
#include "util.h"
#include "point_light.h"

using std::initializer_list;
using std::get;
using std::vector;

namespace raycasting
{
	raytracer::raytracer(const camera &cam, const initializer_list<const shape*> &shapes, const material &curr_mat,
		std::shared_ptr<realtime_drawing_manager> drm, const vector<point_light*>& lts, double sd) : renderer(cam, shapes, curr_mat, drm)
	{
		this->lights = lts;
		this->shadow_darkness = sd;
	}

	color raycasting::raytracer::radiance(int current_depth, int depth, const ray &curr_ray, const material &surrounding_mat, int diffuse_depth) const
	{
		if (current_depth == depth)
			return color(0, 0, 0);

		//intersect with objects
		auto res = util::intersectObjects(shapes, curr_ray, false, false);
		auto intrs = get<0>(res);
		if (!intrs.valid) //no intersections
			return environment_mat.col;

		if (intrs.mat.emissivity > 0)
			return intrs.mat.col * intrs.mat.emissivity;

		//diffuse component
		color diffuse(0, 0, 0);
		if (intrs.mat.diffuseness > 0)
		{
			for (auto &light : lights)
			{
				//define ray from hit object to light
				ray light_dir(intrs.point, (light->point - intrs.point).normalize());
				double nl = light_dir.direction*intrs.normal;
				double diminish_coeff = 1.0;
				double dist = intrs.point.sqrDistance(light->point);
				//check whether it reaches the light
				for (int i = 0; i < (int)shapes.size(); ++i)
				{
					shape::intersection temp_intrs(shapes[i]->intersect(light_dir, shapes[i]->interpolate_normals));
					if (temp_intrs.valid && temp_intrs.point.sqrDistance(intrs.point) < dist)
					{
						diminish_coeff *= shadow_darkness;
						break;
					}
				}
				diffuse += intrs.mat.diffuseness * intrs.mat.col * light->light_color * light->light_intensity * abs(nl)*diminish_coeff;
			}
		}

		//reflection
		color reflected(0, 0, 0);
		if (intrs.mat.reflectivity > 0)
			reflected = intrs.mat.reflectivity * intrs.mat.col * radiance(current_depth + 1, depth, ray::ray(intrs.point, ((-curr_ray.direction).reflect(intrs.normal)).normalize()), environment_mat, diffuse_depth);

		//refraction
		color refracted(0, 0, 0);
		if (intrs.mat.transparency > 0)
		{
			vector3 b = (-curr_ray.direction) % intrs.normal;
			double sin_theta_one = b.magnitude();
			double n1 = surrounding_mat.refraction_index, n2 = intrs.mat.refraction_index;
			double sin_theta_two = n1 / n2 * sin_theta_one;
			//total internal reflection
			if (sin_theta_two > 1)
				refracted = radiance(current_depth + 1, depth, ray::ray(intrs.point, ((-curr_ray.direction).reflect(intrs.normal)).normalize()), environment_mat, diffuse_depth);
			else
				refracted = radiance(current_depth + 1, depth, ray::ray(intrs.point, ((-intrs.normal).rotate(b, -asin(sin_theta_two))).normalize()), intrs.inside ? surrounding_mat : intrs.mat, diffuse_depth);
			refracted *= (intrs.mat.transparency * intrs.mat.col);
		}

		return (diffuse + reflected + refracted);
	}
}
