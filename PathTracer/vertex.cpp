#include "vertex.h"

namespace raycasting
{
	raycasting::vertex::vertex(const vector3 &coordinates, const vertex_data &data) : coords(coordinates), data(data) { }
}
