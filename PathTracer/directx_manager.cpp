#include "directx_manager.h"
#include "pixel.h"

namespace raycasting
{
	bool directx_manager::initializeDirect3d11App(HINSTANCE hInstance)
	{
		//describe our buffer
		DXGI_MODE_DESC bufferDesc;

		ZeroMemory(&bufferDesc, sizeof(DXGI_MODE_DESC));

		bufferDesc.Width = width;
		bufferDesc.Height = height;
		bufferDesc.RefreshRate.Numerator = 60;
		bufferDesc.RefreshRate.Denominator = 1;
		bufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

		//describe our swap chain
		DXGI_SWAP_CHAIN_DESC swapChainDesc;

		ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

		swapChainDesc.BufferDesc = bufferDesc;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.BufferCount = 1;
		swapChainDesc.OutputWindow = hwnd;
		swapChainDesc.Windowed = TRUE;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;


		//create our swap chain
		D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, NULL, NULL,
			D3D11_SDK_VERSION, &swapChainDesc, &swap_chain, &d3d11_device, NULL, &d3d11_dev_con);

		//create our back buffer
		//ID3D11Texture2D* BackBuffer;
		swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&back_buffer);

		//create our render target
		d3d11_device->CreateRenderTargetView(back_buffer, NULL, &render_target_view);
		back_buffer->Release();

		//Set our Render Target
		d3d11_dev_con->OMSetRenderTargets(1, &render_target_view, NULL);

		return true;
	}

	void directx_manager::releaseObjects()
	{
		//release the COM objects we created
		swap_chain->Release();
		d3d11_device->Release();
		d3d11_dev_con->Release();
	}

	bool directx_manager::initializeWindow(HINSTANCE hInstance, int ShowWnd, int width, int height, bool windowed)
	{
		typedef struct _WNDCLASS {
			UINT cbSize;
			UINT style;
			WNDPROC lpfnWndProc;
			int cbClsExtra;
			int cbWndExtra;
			HANDLE hInstance;
			HICON hIcon;
			HCURSOR hCursor;
			HBRUSH hbrBackground;
			LPCTSTR lpszMenuName;
			LPCTSTR lpszClassName;
		} WNDCLASS;

		WNDCLASSEX wc;

		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = wndProc;
		wc.cbClsExtra = NULL;
		wc.cbWndExtra = NULL;
		wc.hInstance = hInstance;
		wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 2);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = this->wnd_class_name;
		wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

		if (!RegisterClassEx(&wc))
		{
			MessageBox(NULL, "Error registering class",
				"Error", MB_OK | MB_ICONERROR);
			return 1;
		}

		hwnd = CreateWindowEx(
			NULL,
			wnd_class_name,
			"Real-time renderer",
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT,
			width, height,
			NULL,
			NULL,
			hInstance,
			NULL
		);

		if (!hwnd)
		{
			MessageBox(NULL, "Error creating window",
				"Error", MB_OK | MB_ICONERROR);
			return 1;
		}

		ShowWindow(hwnd, ShowWnd);
		UpdateWindow(hwnd);

		return true;
	}

	LRESULT directx_manager::wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_KEYDOWN:
			if (wParam == VK_ESCAPE) {
				DestroyWindow(hwnd);
			}
			return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		}
		return DefWindowProc(hwnd,
			msg,
			wParam,
			lParam);
	}

	directx_manager::directx_manager(int w, int h, HINSTANCE hi) : width(w), height(h), h_instance(hi)
	{
		hwnd = NULL;
		wnd_class_name = "window";
		data = new char[4 * width * height];
	}

	directx_manager::~directx_manager()
	{
		delete[] data;
	}

	void directx_manager::start()
	{
		//win32 showcommand --> 5 == "show window in current size and position"
		if (!initializeWindow(h_instance, 5, width, height, true))
		{
			MessageBox(0, "Window Initialization - Failed",
				"Error", MB_OK);
		}

		if (!initializeDirect3d11App(h_instance))    //Initialize Direct3D
		{
			MessageBox(0, "Direct3D Initialization - Failed",
				"Error", MB_OK);
		}
		running = true;
	}

	void directx_manager::draw(const std::vector<std::vector<color>> img, int num_iterations)
	{
		// create our BackBuffer
		if (back_buffer == nullptr)
			swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&back_buffer);

		color col;
		for (int i = 0; i < width; ++i)
			for (int j = 0; j < height; ++j)
			{
				col = img[i][j];
				pixel pixel(col, num_iterations);
				data[4 * (j*width + i)] = pixel.R;
				data[4 * (j*width + i) + 1] = pixel.G;
				data[4 * (j*width + i) + 2] = pixel.B;
				data[4 * (j*width + i) + 3] = (char)255;
			}
		d3d11_dev_con->UpdateSubresource(back_buffer, 0, nullptr, data, 4 * width, 1);

		//BackBuffer->Release();

		//Present the backbuffer to the screen
		swap_chain->Present(0, 0);
	}

	void directx_manager::messageloop() {
		MSG msg;
		ZeroMemory(&msg, sizeof(MSG));
		while (running)
		{
			BOOL PeekMessageL(
				LPMSG lpMsg,
				HWND hWnd,
				UINT wMsgFilterMin,
				UINT wMsgFilterMax,
				UINT wRemoveMsg
			);

			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
					break;
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	void directx_manager::stop()
	{
		releaseObjects();
		running = false;
	}
}