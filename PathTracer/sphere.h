#pragma once
#include "shape.h"

namespace raycasting
{
	class sphere : public shape
	{
	private:
		vector3 center;
		double radius;
	public:
		sphere() = default;
		sphere(const vector3&, double, const material&);
		intersection intersect(const ray&, bool) const override;
	};
}
