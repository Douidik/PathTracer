#include "image.h"

using std::vector;

namespace raycasting
{
	raycasting::image::image(int width, int heigth) : width(width), height(heigth), img(vector<vector<color>>(width, vector<color>(height))) { }

	void raycasting::image::write(const char* filename, int num_iterations) const
	{
		pixel* array = new pixel[width*height];
		for (int i = 0; i < width; ++i)
			for (int j = 0; j < height; ++j)
				array[j*width+i] = pixel(img[i][j], num_iterations);
		FILE *fp = fopen(filename, "wb");
		fprintf(fp, "P6\n%d %d\n255\n", width, height);
		fwrite(array, sizeof(pixel), width*height, fp);
		fclose(fp);
		delete array;
	}
}
