#pragma once
#include "shape.h"

namespace raycasting
{
	struct point_light
	{
		vector3 point;
		color light_color;
		double light_intensity;
		point_light() = default;
		point_light(const vector3&, const color&, double);
	};
}
