#include "pixel.h"

namespace raycasting
{
	raycasting::pixel::pixel(unsigned char r, unsigned char g, unsigned char b) : R(r), G(g), B(b) { }

	pixel::pixel(const color &c, int num_iterations) : R(clamp(adjust(c.R, num_iterations))), G(clamp(adjust(c.G, num_iterations))), B(clamp(adjust(c.B, num_iterations))) { }

	unsigned char pixel::clamp(double x) const
	{
		return (unsigned char)(x > 255 ? 255 : x);
	}

	double pixel::adjust(double x, int num_iterations) const
	{
		return (x / num_iterations * 255);
	}
}
